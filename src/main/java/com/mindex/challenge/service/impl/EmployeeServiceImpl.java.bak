package com.mindex.challenge.service.impl;

import com.mindex.challenge.dao.EmployeeRepository;
import com.mindex.challenge.data.Employee;
import com.mindex.challenge.data.ReportingStructure;
import com.mindex.challenge.service.EmployeeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private static final Logger LOG = LoggerFactory.getLogger(EmployeeServiceImpl.class);

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public Employee create(Employee employee) {
        LOG.debug("Creating employee [{}]", employee);

        employee.setEmployeeId(UUID.randomUUID().toString());
        employeeRepository.insert(employee);

        return employee;
    }

    @Override
    public Employee read(String id) {
        LOG.debug("Creating employee with id [{}]", id);

        Employee employee = employeeRepository.findByEmployeeId(id);

        if (employee == null) {
            throw new RuntimeException("Invalid employeeId: " + id);
        }

        return employee;
    }

    @Override
    public Employee update(Employee employee) {
        LOG.debug("Updating employee [{}]", employee);

        return employeeRepository.save(employee);
    }
    
    @Override
    public ReportingStructure getReportingStructure(String id){
    		LOG.debug("Getting reporting structure for employee id [{}]", id);
    		
    		Employee employee = employeeRepository.findByEmployeeId(id);

        if (employee == null) {
            throw new RuntimeException("Invalid employeeId: " + id);
        }
        
        ReportingStructure reportingStructure = new ReportingStructure(employee);
        
        return reportingStructure;
    }
    
    /**
     * Get the number of employees that report to this employee.
     */
    private int numReports(Employee employee){
    		
    		int numberReports = 0;
    		
    		int numberDirectReports = 0;
    		int numberSubReports = 0;
    		
    		//first get all direct reports...
    		List<Employee> directReports = employee.getDirectReports();
    		if(directReports != null){
    				numberDirectReports = directReports.size();
    				
    				//then get all of their reports
		    		for(Employee emp : directReports){
		    				//need to re-query each employee, otherwise their reports come up null
		    				Employee queriedEmp = employeeRepository.findByEmployeeId(emp.getEmployeeId());
		    				numberSubReports += numReports(queriedEmp); //add their reports...
		    		}
    		}
    		
    		
    		
    		//total number of reports = direct reports + their reports
    		numberReports = numberDirectReports + numberSubReports;
    		
    		return numberReports;
    }
}
