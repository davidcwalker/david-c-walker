package com.mindex.challenge.data;


public class ReportingStructure {
    
    private Employee employee;
    private int numberOfReports;

    public ReportingStructure(Employee employee, int numReports) {
    	
    		this.employee = employee;
    		
    		numberOfReports = numReports;
    	
    }

    public Employee getEmployee() {
        return employee;
    }

    public int getNumberOfReports() {
    		return numberOfReports;
    }
    
    /**
     * Get the number of employees that report to this employee.
     */
    /*private int numReports(Employee employee){
    		
    		int numberReports = 0;
    		
    		int numberDirectReports = 0;
    		int numberSubReports = 0;
    		
    		//first get all direct reports...
    		List<Employee> directReports = employee.getDirectReports();
    		if(directReports != null){
    				numberDirectReports = directReports.size();
    				
    				//then get all of their reports
		    		for(Employee emp : directReports){
		    				//need to re-query each employee, otherwise their reports come up null
		    				Employee queriedEmp = EmployeeRepository.findByEmployeeId(emp.getEmployeeId());
		    				numberSubReports += numReports(queriedEmp); //add their reports...
		    		}
    		}
    		
    		
    		
    		//total number of reports = direct reports + their reports
    		numberReports = numberDirectReports + numberSubReports;
    		
    		return numberReports;
    }*/
}
