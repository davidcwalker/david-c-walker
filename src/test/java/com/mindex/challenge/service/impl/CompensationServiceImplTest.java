package com.mindex.challenge.service.impl;

import com.mindex.challenge.data.Employee;
import com.mindex.challenge.data.Compensation;
import com.mindex.challenge.service.EmployeeService;
import com.mindex.challenge.service.CompensationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Date;
import java.util.Calendar;

import org.joda.money.Money;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CompensationServiceImplTest {

    private String employeeUrl;
    private String employeeIdUrl;
    
    private String compensationUrl;
    private String compensationEmployeeIdUrl;
    

    @Autowired
    private EmployeeService employeeService;
    
    @Autowired
    private CompensationService compensationService;

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Before
    public void setup() {
        employeeUrl = "http://localhost:" + port + "/employee";
        employeeIdUrl = "http://localhost:" + port + "/employee/{id}";
        
        compensationUrl = "http://localhost:" + port + "/compensation";
        compensationEmployeeIdUrl = "http://localhost:" + port + "/compensation/{employeeId}";
        
    }
    
    @Test
    public void testCreateRead() {
    	//replicate employee test
    	Employee testEmployee = new Employee();
        testEmployee.setFirstName("Hank");
        testEmployee.setLastName("Hill");
        testEmployee.setDepartment("Propane");
        testEmployee.setPosition("Assistant Manager");

        // Create checks
        Employee createdEmployee = restTemplate.postForEntity(employeeUrl, testEmployee, Employee.class).getBody();
        assertNotNull(createdEmployee.getEmployeeId());
        assertEmployeeEquivalence(testEmployee, createdEmployee);
        
        Calendar myCalendar = Calendar.getInstance();
        myCalendar.set(2019, 0, 1);
        
        Date effectiveDate = myCalendar.getTime();
        
        Money salary = Money.parse("USD 75000.00");
        
        Compensation comp = new Compensation();
        comp.setEffectiveDate(effectiveDate);
        comp.setEmployeeId(createdEmployee.getEmployeeId());
        comp.setSalary(salary.toString());
        
        Compensation createdComp = restTemplate.postForEntity(compensationUrl, comp, Compensation.class).getBody();
        assertNotNull(createdComp);
        assertNotNull(createdComp.getEmployeeId());
        assertNotNull(createdComp.getEffectiveDate());
        assertNotNull(createdComp.getSalary());
        assertCompensationEquivalence(comp, createdComp);
        
        Compensation readComp = restTemplate.getForEntity(compensationEmployeeIdUrl, 
        						Compensation.class, comp.getEmployeeId()).getBody();
        assertCompensationEquivalence(comp, readComp);
    }

    private static void assertEmployeeEquivalence(Employee expected, Employee actual) {
        assertEquals(expected.getFirstName(), actual.getFirstName());
        assertEquals(expected.getLastName(), actual.getLastName());
        assertEquals(expected.getDepartment(), actual.getDepartment());
        assertEquals(expected.getPosition(), actual.getPosition());
    }
    
    private static void assertCompensationEquivalence(Compensation expected, Compensation actual) {
        assertEquals(expected.getEmployeeId(), actual.getEmployeeId());
        assertEquals(expected.getEffectiveDate(), actual.getEffectiveDate());
        assertEquals(expected.getSalary(), actual.getSalary());
    }
}
